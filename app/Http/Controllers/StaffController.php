<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class StaffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        
        $staff = User::select('users.*','user_details.name','user_details.telephone','role.role')
            ->join('role','users.role','role.id')
            ->join('user_details','users.id','user_details.user_id')
            ->whereIn('role.id',['1','2','3'])
            ->paginate(10);
        return view('staff.index',compact('staff'));
    }

    public function userLog($id) {
        $userLog = User::join('user_log','users.id','user_log.user_id')->where('users.id',$id)->paginate(10);
        return view('staff.log',compact(['userLog']));
    }

    public function show() {
        
    }

    public function siteLive($id,$status) {
    $userLiveStatus = User::where('id',$id)->update(['site_live'=>$status]);
    if($userLiveStatus) {
        return redirect()->back()->withSuccess(['successMessage'=>trans('message.staff_site_live_success')]);
    } else {
        return redirect()->back()->withErrors(['errorMessage' => trans('message.staff_site_live_failed')]);
    }
  }

    public function roundRobin($id,$status) {
        $userLiveStatus = User::where('id',$id)->update(['agent_live'=>$status]);
        if($userLiveStatus) {
            return redirect()->back()->withSuccess(['successMessage'=>trans('message.staff_round_robin_success')]);
        } else {
            return redirect()->back()->withErrors(['errorMessage' => trans('message.staff_round_robin_failed')]);
        }
    }

}
