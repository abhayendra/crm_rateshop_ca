<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $leads = User::select('users.*','user_details.name','user_details.telephone','role.role')
            ->join('role','users.role','role.id')
            ->join('user_details','users.id','user_details.user_id')
            ->where('role.id','6')
            ->paginate(10);
        return view('lead.index',compact('leads'));
    }

    public function userLog($id) {
        $userLog = User::join('user_log','users.id','user_log.user_id')->where('users.id',$id)->paginate(10);
        return view('lead.log',compact(['userLog']));
    }

    public function export() {

    } 

}
