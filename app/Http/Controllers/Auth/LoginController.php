<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    protected $redirectTo = '/dashboard';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function authenticate(LoginRequest $request)
    {
        $user = User::where('username', $request->username)
            ->where('password',md5($request->password))
            ->first();
        if($user) {
            Auth::login($user);
            return redirect('/dashboard');
        } else {
         return redirect()->back()->withErrors(['errorMessage'=>trans('auth.failed')]);
        }
    }
}
