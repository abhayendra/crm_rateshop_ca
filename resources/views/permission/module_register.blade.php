@extends('layouts.app')
@section('content')
    @foreach($routes as $route)
        @php $routeName[] = $route->getName() @endphp
    @endforeach

    @php
       $noNeed = Array('login','logout','register','password.request','password.email','password.reset','password.update','dashboard','debugbar.openhandler','debugbar.clockwork','debugbar.telescope','debugbar.assets.css','debugbar.assets.js' ,'debugbar.cache.delete','' );
       $finalArray = array_diff($routeName,$noNeed);
    @endphp

    <div class="row">
        <div class="col-md-12">
                    {!! Form::open(['url'=>'save_module_register']) !!}
                        <div class="card card-default">
                            <div class="card-header">
                                <div class="card-title"></div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label class="col-form-label">Module Name *</label>
                                    <input class="form-control" type="text" name="module_name" required="">
                                </div>
                                <table class="table table-striped table-hover table-bordered">
                                    <thead>
                                    <tr class="active">
                                        <th>View</th>
                                        <th>Create</th>
                                        <th>Read</th>
                                        <th>Update</th>
                                        <th>Delete</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($finalArray as $key=>$routeName)
                                    <tr>
                                        <td align="left" class="active" align="center"><input type="checkbox" class="is_visible" name="is_visible[]" value="{!! $routeName !!}"> {!! $routeName !!}</td>
                                        <td align="left" class="warning" align="center"><input type="checkbox" class="is_create" name="is_create[]" value="{!! $routeName !!}"> {!! $routeName !!}</td>
                                        <td align="left" class="info" align="center"><input type="checkbox" class="is_read" name="is_read[]" value="{!! $routeName !!}"> {!! $routeName !!}</td>
                                        <td align="left" class="success" align="center"><input type="checkbox" class="is_edit" name="is_edit[]" value="{!! $routeName !!}"> {!! $routeName !!}</td>
                                        <td align="left" class="danger" align="center"><input type="checkbox" class="is_delete" name="is_delete[]" value="{!! $routeName !!}"> {!! $routeName !!}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-footer">
                                <div class="clearfix">
                                    <div class="float-right"><button class="btn btn-primary" type="submit">Register</button></div>
                                </div>
                            </div>
                        </div><!-- END card-->
                   {!! Form::close() !!}
        </div>
    </div>
@endsection
