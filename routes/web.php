<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@authenticate');
Route::get('/logout', 'Auth\LoginController@logout');
Route::get('/dashboard','DashboardController@dashboard');
Route::get('/module_register','PermissionController@moduleRegister');
Route::post('/save_module_register','PermissionController@saveModuleRegister');
Route::get('/privileges','PermissionController@privileges');
Route::post('/save_privileges','PermissionController@savePrivileges');

//user module
Route::get('users','UserController@index')->name('view_users');
Route::get('user/{id}','UserController@show')->name('show_user');
Route::get('user/{id}/edit','UserController@edit')->name('edit_user');
Route::put('user/update','UserController@update')->name('update_user');
Route::Delete('user/{id}','UserController@delete')->name('delete_user');
Route::get('users/export','UserController@export')->name('export_users_record');
Route::post('user/round_robin/{id}/{status}','UserController@roundRobin')->name('change_round_robin');
Route::post('user/site_live/{id}/{status}','UserController@siteLive')->name('change_site_live');
Route::get('user/log/{id}','UserController@userLog')->name('user_log');


//lead module
Route::get('leads','LeadController@index')->name('view_leads');
Route::get('lead/log/{id}','LeadController@leadLog')->name('lead_log');

//lead module
Route::get('staff','StaffController@index')->name('view_staff');
Route::get('staff/log/{id}','StaffController@leadLog')->name('staff_log');

//Setting
Route::get('profile','SettingController@profile');
Route::post('profile_update','SettingController@profileUpdate');
Route::post('change_password','SettingController@changePassword');
