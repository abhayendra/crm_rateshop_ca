<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <title>{!! trans('setting.defaultTitle') !!}</title>
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/brands.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/regular.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/solid.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/fontawesome.css') !!}
    {!! Html::style('resources/assets/vendor/simple-line-icons/css/simple-line-icons.css') !!}
    {!! Html::style('resources/assets/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/css/app.css') !!}
</head>
<body>
<div class="wrapper">
    <div class="abs-center wd-xl">
        <div class="text-center mb-6">
            <div class="mb-6"><em class="fa fa-wrench fa-5x text-muted"></em></div>
            <div class="text-lg mb-6">500</div>
            <p class="lead m-0">Oh! Something went wrong :(</p>
            <p>Don't worry, we're now checking this.</p>
            <p>In the meantime, please try one of those links below or come back in a moment</p>
        </div>
        <div class="input-group mb-4"><input class="form-control" type="text" placeholder="Try with a search"><span class="input-group-btn"><button class="btn btn-secondary" type="button"><em class="fa fa-search"></em></button></span></div>
        <ul class="list-inline text-center text-sm mb-4">
            <li class="list-inline-item"><a class="text-muted" href="{!! url('dashboard') !!}">Go to App</a></li>
            <li class="text-muted list-inline-item">|</li>
            <li class="list-inline-item"><a class="text-muted" href="{!! url('/') !!}">Login</a></li>
        </ul>
        <div class="p-3 text-center"><span class="mr-2">&copy;</span><span>{!! date('Y') !!} </span><span class="mr-2">-</span><span> {!! trans('setting.appName') !!}</span></div>
    </div>
</div>
{!! Html::script('resources/assets/vendor/modernizr/modernizr.custom.js') !!}
{!! Html::script('resources/assets/vendor/js-storage/js.storage.js') !!}
{!! Html::script('resources/assets/vendor/i18next/i18next.js') !!}
{!! Html::script('resources/assets/vendor/i18next-xhr-backend/i18nextXHRBackend.js') !!}
{!! Html::script('resources/assets/vendor/jquery/dist/jquery.js') !!}
{!! Html::script('resources/assets/vendor/popper.js/dist/umd/popper.js') !!}
{!! Html::script('resources/assets/vendor/bootstrap/dist/js/bootstrap.js') !!}
{!! Html::script('resources/assets/vendor/parsleyjs/dist/parsley.js') !!}
</body>
</html>
