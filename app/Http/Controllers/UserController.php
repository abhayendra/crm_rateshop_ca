<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index() {
        $users = User::select('users.*','user_details.name','user_details.telephone','role.role')
            ->join('role','users.role','role.id')
            ->join('user_details','users.id','user_details.user_id')
            ->paginate(10);
        return view('users.index',compact('users'));
    }

    public function show($id) {

    }

    public function edit($id) {

    }

    public function update(Request $request) {

    }

    public function delete($id) {

    }

    public function export() {

    }

    public function roundRobin($id,$status) {

    }

    public function siteLive($id,$status) {

    }

    public function userLog($id) {
        $userLog = User::join('user_log','users.id','user_log.user_id')->where('users.id',$id)->paginate(10);
        return view('users.userlog',compact(['userLog']));
    }




}
