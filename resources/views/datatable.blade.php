@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">Export Buttons</div>
            <div class="text-sm">When displaying data in a DataTable, it can often be useful to your end users for them to have the ability to obtain the data from the DataTable, extracting it into a file for them to use locally. This can be done with either HTML5 based buttons or Flash buttons.</div>
        </div>
        <div class="card-body">
            <div id="datatable_action" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="dt-buttons btn-group">
                    <button class="btn btn-default btn-info"  type="button"><span>Copy</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>CSV</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Excel</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>PDF</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Print</span></button>
                </div>
                {!! Form::open(['url'=>'','method'=>'get']) !!}
                <div id="datatable_search" class="dataTables_filter">
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::select('fieldType',['All','Engine','Engine2','Engine Platefrom'],'',['class'=>'form-control form-control-sm']) !!}
                            </div>
                            <div class="col-md-4">
                                {!! Form::text('fieldType','',['class'=>'form-control form-control-sm','Placeholder'=>'Search Keyword']) !!}
                            </div>
                        </div>
                </div>
                {!! Form::close() !!}
                <table class="table table-striped my-4 w-100 dataTable no-footer dtr-inline" id="datatable" >
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc">Engine</th>
                        <th class="sorting">Browser</th>
                        <th class="sorting">Platform</th>
                        <th class="sort-numeric sorting">Engine version</th>
                        <th class="sort-alpha sorting">Status</th>
                        <th class="sort-alpha sorting">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @for($i=1; $i<=100;$i++)
                    <tr class="gradeX">
                        <td tabindex="0" class="sorting_1">Trident</td>
                        <td>Internet Explorer 4.0</td>
                        <td>Win 95+</td>
                        <td>4</td>
                        <td>
                            <span class="label label-default">Default</span>
                            <span class="label label-primary">Primary</span>
                            <span class="label label-success">Success</span>
                            <span class="label label-info">Info</span>
                            <span class="label label-warning">Warning</span>
                            <span class="label label-danger">Danger</span>
                        </td>
                        <td>
                            <button class="btn btn-labeled btn-success btn-xs" type="button"><i class="fa fa-check"></i></button>
                            <button class="btn btn-labeled btn-danger btn-xs" type="button"><i class="fa fa-times"></i></button>
                            <button class="btn btn-labeled btn-info btn-xs" type="button"><i class="fa fa-exclamation"></i></button>
                            <button class="btn btn-labeled btn-warning btn-xs" type="button"><i class="fas fa-exclamation-triangle"></i></button>
                        </td>
                    </tr>
                    @endfor
                    </tbody>
                </table>
                <div class="dataTables_info">Showing 1 to 10 of 100 entries</div>
                <div class="dataTables_paginate paging_simple_numbers">
                    <ul class="pagination">
                        <li class="paginate_button page-item previous disabled" id="datatable2_previous"><a href="#" aria-controls="datatable2" data-dt-idx="0" tabindex="0" class="page-link"><em class="fa fa-caret-left"></em></a></li>
                        <li class="paginate_button page-item active"><a href="#" aria-controls="datatable2" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="datatable2" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="datatable2" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="datatable2" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="datatable2" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                        <li class="paginate_button page-item disabled" id="datatable2_ellipsis"><a href="#" aria-controls="datatable2" data-dt-idx="6" tabindex="0" class="page-link">…</a></li>
                        <li class="paginate_button page-item "><a href="#" aria-controls="datatable2" data-dt-idx="7" tabindex="0" class="page-link">10</a></li>
                        <li class="paginate_button page-item next" id="datatable2_next"><a href="#" aria-controls="datatable2" data-dt-idx="8" tabindex="0" class="page-link"><em class="fa fa-caret-right"></em></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
