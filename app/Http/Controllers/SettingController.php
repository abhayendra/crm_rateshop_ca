<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class SettingController extends Controller
{
    public function profile() {
        $user = User::select('users.*','user_details.*','role.role')
            ->join('role','users.role','role.id')
            ->join('user_details','users.id','user_details.user_id')
            ->find(Auth::user()->id);
        return view('setting.profile',compact('user'));
    }

    public function profileUpdate(Request $request) {
        $this->validate($request, [
            'profile_pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('profile_pic')) {
            $image = $request->file('profile_pic');
            $profileImageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/upload/user_profile');
            $image->move($destinationPath, $profileImageName);
        }

       $userUpdate = User::where('id',$request->user_id)
           ->update(['username'=>$request->username,'email'=>$request->email]);
       $userDetailUpdate = UserDetail::where('user_id',$request->user_id)
           ->update([
               'name'=>$request->name,
               'telephone'=> $request->telephone,
               'fulladdr' => $request->fulladdr,
               'city' => $request->city,
               'province' => $request->province,
               'country' => $request->country,
               'zipcode' => $request->zipcode,
               'profile_pic' => $profileImageName,
           ]);


        if($userUpdate && $userDetailUpdate) {
            return redirect()->back()->withSuccess(['successMessage'=>trans('auth.failed')]);
        } else {
            return redirect()->back()->withErrors(['errorMessage'=>trans('auth.failed')]);
        }

       return redirect()->back();
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        if(Auth::Check()){
         $passwordUpdate = User::where('password',md5($request->oldpass))->update(['password'=>md5($request->password)]);
            if($passwordUpdate) {
                return redirect()->back()->withSuccess(['successMessage'=>trans('auth.failed')]);
            } else {
                return redirect()->back()->withErrors(['errorMessage'=>trans('auth.failed')]);
            }
        } else {
            return redirect('/');
        }
    }
}
