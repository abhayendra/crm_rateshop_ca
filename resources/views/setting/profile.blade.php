@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="card b">
                <div class="card-header bg-gray-lighter text-bold">Personal Settings</div>
                <div class="list-group">
                    <a class="list-group-item list-group-item-action active" href="#tabSetting1" data-toggle="tab">Profile</a>
                    <a class="list-group-item list-group-item-action" href="#tabSetting2" data-toggle="tab">Change Password</a>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="tab-content p-0 b0">
                <div class="tab-pane active" id="tabSetting1">
                    <div class="card b">
                        <div class="card-body">
                            {!! Form::open(['url'=>'profile_update']) !!}!
                                <input type="hidden" name="user_id" value="{!! $user->user_id !!}">
                                <div class="form-group">
                                    <label>Username</label>
                                    {!! Form::text('username',$user->username,['class'=>'form-control','readonly'=>'true']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Full Name </label>
                                    {!! Form::text('name',$user->name,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    {!! Form::text('email',$user->email,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    {!! Form::text('telephone',$user->telephone,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    {!! Form::text('fulladdr',$user->fulladdr,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>City</label>
                                    {!! Form::text('city',$user->city,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Province</label>
                                    {!! Form::text('province',$user->province,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>country</label>
                                    {!! Form::text('country',$user->country,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Zipcode</label>
                                    {!! Form::text('zipcode',$user->zipcode,['class'=>'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Picture</label>
                                    {!! Form::file('profile_pic',['class'=>'form-control filestyle']) !!}
                                </div>
                                <button class="btn btn-info" type="submit">Update settings</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tabSetting2">
                    <div class="card b">
                        <div class="card-body">
                            {!! Form::open(['url'=>'change_password']) !!}
                                <div class="form-group">
                                    <label>Current password</label>
                                    <input class="form-control" type="password" name="oldpass">
                                </div>
                                <div class="form-group">
                                    <label>New password</label>
                                    <input class="form-control" type="password" name="password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm new password</label>
                                    <input class="form-control" type="password" name="password_confirmation">
                                </div>
                                <button class="btn btn-info" type="submit">Update password</button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection