<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="Bootstrap Admin App">
    <meta name="keywords" content="app, responsive, jquery, bootstrap, dashboard, admin">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <title>{!! trans('setting.defaultTitle') !!}</title>
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/brands.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/regular.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/solid.css') !!}
    {!! Html::style('resources/assets/vendor/fortawesome/fontawesome-free/css/fontawesome.css') !!}
    {!! Html::style('resources/assets/vendor/simple-line-icons/css/simple-line-icons.css') !!}
    {!! Html::style('resources/assets/css/bootstrap.css') !!}
    {!! Html::style('resources/assets/css/app.css') !!}
</head>
<body>
<div class="wrapper">
    <div class="block-center mt-4" style="width:100%">
        <!-- START card-->
        <div class="text-center"><a href="#">{!! Html::image('resources/assets/img/logo.png','Logo',['class'=>'block-center rounded']) !!}</a></div>
    <div class="card card-flat card-login-box">
        <div class="card-header card-login-header"><span style="color: #233188;font-weight: bold;">Welcome</span>, Sign in to Continue</div>
        <div class="card-body">
            {!! Form::open(['url'=>'login','id'=>'loginForm']) !!}
            <div class="form-group">
                <label>Enter your Password</label>
                <div class="input-group with-focus">
                    <input class="form-control border-right-0 @error('username') is-invalid @enderror " id="username"  name="username"  type="text" placeholder="Enter email" autocomplete="off" >
                    <div class="input-group-append">
                        <span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-envelope"></em></span>
                    </div>
                </div>
                @if ($errors->has('errorMessage'))
                    <div class="parsley-required">{{ $errors->first('errorMessage') }}</div>
                @endif
                @if ($errors->has('username'))
                    <div class="parsley-required">{{ $errors->first('username') }}</div>
                @endif
            </div>
            <div class="form-group">
                <label>Enter your Email-Address</label>
                <div class="input-group with-focus">

                    <input class="form-control border-right-0 @error('password') is-invalid @enderror" id="password" name="password" type="password" placeholder="Password" >
                    <div class="input-group-append">
                        <span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-lock"></em></span>
                    </div>
                </div>
                @if ($errors->has('password'))
                    <div class="parsley-required">{{ $errors->first('password') }}</div>
                @endif
            </div>
            <div class="clearfix">
                <div class="checkbox c-checkbox float-left mt-0"><label><input type="checkbox" value="" name="remember"><span class="fa fa-check"></span> Remember Me</label></div>
                <div class="float-right"><a class="text-muted" href="{!! route('password.request') !!}">Forgot your password?</a></div>
            </div><button class="btn btn-block btn-primary mt-3" type="submit" style="font-size:17px;">Login</button>
        {!! Form::close() !!}
        <!--<p class="pt-3 text-center">Need to Signup?</p><a class="btn btn-block btn-secondary" href="register.html">Register Now</a>-->
        </div>
    </div><!-- END card-->
    <div class="p-3 text-center"><span class="mr-2">&copy;</span><span>{!! date('Y') !!} </span><span class="mr-2">-</span><span> {!! trans('setting.appName') !!}</span></div>
</div>
</div>
{!! Html::script('resources/assets/vendor/modernizr/modernizr.custom.js') !!}
{!! Html::script('resources/assets/vendor/js-storage/js.storage.js') !!}
{!! Html::script('resources/assets/vendor/i18next/i18next.js') !!}
{!! Html::script('resources/assets/vendor/i18next-xhr-backend/i18nextXHRBackend.js') !!}
{!! Html::script('resources/assets/vendor/jquery/dist/jquery.js') !!}
{!! Html::script('resources/assets/vendor/popper.js/dist/umd/popper.js') !!}
{!! Html::script('resources/assets/vendor/bootstrap/dist/js/bootstrap.js') !!}
{!! Html::script('resources/assets/vendor/parsleyjs/dist/parsley.js') !!}
</body>
</html>
