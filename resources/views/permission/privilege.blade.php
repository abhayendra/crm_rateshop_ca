@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
        <div class="card card-default">
            <div class="card-body">
                {!! Form::open(['url'=>'save_privileges']) !!}
                    <div class="box-body">
                        <div class="form-group">
                            <label>Role</label>
                            {!! Form::select('role_id',$roles,old('role_id'),['class'=>'form-control','required'=>'']) !!}
                        </div>
                        <div id="privileges_configuration" class="form-group">
                            <label>Privileges Configuration</label>
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr class="active">
                                    <th width="3%">No.</th>
                                    <th width="60%">Module's Name</th>
                                    <th>Show on Menu </th>
                                    <th>View</th>
                                    <th>Create</th>
                                    <th>Read</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                                <tr class="info">
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <td align="center"><input title="Check all vertical" type="checkbox" id="is_visible"></td>
                                    <td align="center"><input title="Check all vertical" type="checkbox" id="is_create"></td>
                                    <td align="center"><input title="Check all vertical" type="checkbox" id="is_read"></td>
                                    <td align="center"><input title="Check all vertical" type="checkbox" id="is_edit"></td>
                                    <td align="center"><input title="Check all vertical" type="checkbox" id="is_delete"></td>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i=1; @endphp
                                @foreach($moduleName as $moduleId=>$moduleName)
                                <tr>
                                    <td>{!! $i !!}</td>
                                    <td>{!! $moduleName  !!}</td>
                                    <td class="info" align="center"><input type="checkbox" class="select_horizontal" value="1" name="privileges[{!! $moduleId !!}][sitemenu]"></td>
                                    <td class="active" align="center"><input type="checkbox" class="is_visible" value="1" name="privileges[{!! $moduleId !!}][view]" ></td>
                                    <td class="warning" align="center"><input type="checkbox" class="is_create" value="1" name="privileges[{!! $moduleId !!}][create]" ></td>
                                    <td class="info" align="center"><input type="checkbox" class="is_read" value="1" name="privileges[{!! $moduleId !!}][read]" ></td>
                                    <td class="success" align="center"><input type="checkbox" class="is_edit"  value="1"name="privileges[{!! $moduleId !!}][edit]" ></td>
                                    <td class="danger" align="center"><input type="checkbox" class="is_delete" value="1" name="privileges[{!! $moduleId !!}][delete]"></td>
                                </tr>
                                @php $i++ @endphp
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer" align="right">
                        <button type="button" onclick="location.href='{!! url('/privileges') !!}'" class="btn btn-default">Cancel</button>
                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-save"></i> Save</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        </div>
@endsection
