<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use App\Module;

class PermissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function moduleRegister() {
        $app = app();
        $routes = $app->routes->getRoutes();
        return view('permission.module_register',compact('routes'));
    }

    public function moduleList() {
        return view();
    }

    public function saveModuleRegister(Request $request) {
        $module = new Module;
        $module->module_name = $request->module_name;
        $module->is_visible = json_encode($request->is_visible);
        $module->is_read = json_encode($request->is_read);
        $module->is_edit = json_encode($request->is_edit);
        $module->is_create = json_encode($request->is_create);
        $module->is_delete = json_encode($request->is_delete);

        if($module->save()) {
            return redirect()->back()->withSuccess(['successMessage'=>trans('auth.failed')]);
        } else {
            return redirect()->back()->withErrors(['errorMessage'=>trans('auth.failed')]);
        }
    }

    public function privileges() {
        $roles = Role::where('status',1)->pluck('role','id');
        $moduleName = Module::all()->pluck('module_name','id');
        return view ('permission.privilege',compact('roles','moduleName'));
    }

    public function savePrivileges(Request $request) {
        $permission = new Permission;
        $permission->role_id = $request->role_id;
        foreach($request->privileges as $moduleId=>$action) {
            $permission->module_id = $moduleId;
            $permission->sitemenu_permission = @$action['sitemenu'];
            $permission->view_permission = @$action['view'];
            $permission->create_permission = @$action['create'];
            $permission->read_permission = @$action['read'];
            $permission->edit_permission	 = @$action['edit'];
            $permission->delete_permission = @$action['delete'];
        }
        if($permission->save()) {
            return redirect()->back()->withSuccess(['successMessage'=>trans('auth.failed')]);
        } else {
            return redirect()->back()->withErrors(['errorMessage'=>trans('auth.failed')]);
        }
    }




}
