<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\UserDetail;

class User extends  Authenticatable
{
    use Notifiable;

    protected $table = "users";

    protected $fillable = [
        'username', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userDetail() {
       return $this->hasOne('App\UserDetail');
    }

    public function userLog() {
        return $this->hasMany('App\UserLog','user_id','id');
    }
}
