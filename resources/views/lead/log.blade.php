@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">Export Buttons</div>
            <div class="text-sm">When displaying data in a DataTable, it can often be useful to your end users for them to have the ability to obtain the data from the DataTable, extracting it into a file for them to use locally. This can be done with either HTML5 based buttons or Flash buttons.</div>
        </div>
        <div class="card-body">
            <div id="datatable_action" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="dt-buttons btn-group">
                    <button class="btn btn-default btn-info"  type="button"><span>Copy</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>CSV</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Excel</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>PDF</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Print</span></button>
                </div>
                <table class="table table-striped my-4 w-100 dataTable no-footer dtr-inline" id="datatable" >
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc">Content</th>
                        <th class="sho">Ip</th>
                        <th class="sorting">Country</th>
                        <th class="sorting">Region</th>
                        <th class="sorting">City</th>
                        <th class="sorting">Action Type </th>
                        <th class="">Client Time</th>
                        <th class="">Pltefrom </th>
                        <th class="sort-alpha sorting">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($userLog as $log)
                        <tr class="gradeX">
                            <td>{!! $log->content !!}</td>
                            <td>{!! $log->ip !!}</td>
                            <td>{!! $log->country !!}</td>
                            <td>{!! $log->region_name !!}</td>
                            <td>{!! $log->city !!}</td>
                            <td>{!! $log->action !!}</td>
                            <td>{!! $log->client_time !!}</td>
                            <td>
                                <i class="fa fa-apple"></i><i class="fa fa-windows"></i>
                            </td>
                            <td>
                                <span class="btn btn-labeled btn-info btn-xs"><i class="fa fa-list-ol"></i></span></a>
                                <button class="btn btn-labeled btn-success btn-xs" type="button"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-labeled  btn-info btn-xs" type="button"><i class="fa fa-pen"></i></button>
                                <button class="btn btn-labeled btn-danger btn-xs" type="button"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @php
                    $pageNo = @$_GET['page']==" " ? "0" : @$_GET['page'];
                    $startRecord = $pageNo * 10 == "0" ? "1" : $pageNo * 10+1 ;
                    $endRecord = $pageNo * 10 + 10;
                @endphp
                <div class="dataTables_info">Showing {{$startRecord}} to {{$endRecord}} of {{ $userLog->total()}} entries</div>
                <div class="dataTables_paginate paging_simple_numbers">
                    {{ $userLog->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
