@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-title">Export Buttons</div>
            <div class="text-sm">When displaying data in a DataTable, it can often be useful to your end users for them to have the ability to obtain the data from the DataTable, extracting it into a file for them to use locally. This can be done with either HTML5 based buttons or Flash buttons.</div>
        </div>
        <div class="card-body">
            <div id="datatable_action" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="dt-buttons btn-group">
                    <button class="btn btn-default btn-info"  type="button"><span>Copy</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>CSV</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Excel</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>PDF</span></button>
                    <button class="btn btn-default btn-info"  type="button"><span>Print</span></button>
                </div>
                {!! Form::open(['url'=>'','method'=>'get']) !!}
                <div id="datatable_search" class="dataTables_filter">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::select('fieldType',['All','Engine','Engine2','Engine Platefrom'],'',['class'=>'form-control form-control-sm']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::text('fieldType','',['class'=>'form-control form-control-sm','Placeholder'=>'Search Keyword']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <table class="table table-striped my-4 w-100 dataTable no-footer dtr-inline" id="datatable" >
                    <thead>
                    <tr role="row">
                        <th class="sorting_asc">Name</th>
                        <th class="sorting">Email</th>
                        <th class="sorting">Phone</th>
                        <th class="sorting">Role</th>
                        <th class="sorting">Register Date Time </th>
                        <th class="sort-alpha sorting">Status</th>
                        <th class="sort-alpha sorting">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($staff as $sta)
                        <tr class="gradeX">
                            <td>{!! $sta->name !!}</td>
                            <td>{!! $sta->email !!}</td>
                            <td>{!! $sta->telephone !!}</td>
                            <td>{!! $sta->role !!}</td>
                            <td>{!! date('d M Y h:i',strtotime($sta->created)) !!}</td>
                            <td>
                                @php
                                    $verify_email_class = $sta->is_verify_email == 0 ? "danger":"success";
                                    $verify_phone_class = $sta->is_verify_phone == 0 ? "danger":"success";
                                    $site_live = $sta->site_live == 0 ? "danger":"success";
                                    $agent_live = $sta->agent_live == 0 ? "danger":"success";

                                @endphp
                                <span class="label label-{{$verify_email_class}}">Verify Email</span>
                                <span class="label label-{{$verify_phone_class}}">Verify Phone</span>
                                <br/>
                                <span class="label label-{{$site_live}}">Site Live</span>
                                <span class="label label-{{$agent_live}}">Round Robin</span>
                            </td>
                            <td>
                                <a href="{!! url('staff/log/'.$sta->id) !!}"><span class="btn btn-labeled btn-info btn-xs"><i class="fa fa-list-ol"></i></span></a>
                                <button class="btn btn-labeled btn-success btn-xs" type="button"><i class="fa fa-eye"></i></button>
                                <button class="btn btn-labeled  btn-info btn-xs" type="button"><i class="fa fa-pen"></i></button>
                                <button class="btn btn-labeled btn-danger btn-xs" type="button"><i class="fas fa-trash"></i></button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @php
                    $pageNo = @$_GET['page']==" " ? "0" : @$_GET['page'];
                    $startRecord = $pageNo * 10 == "0" ? "1" : $pageNo * 10+1 ;
                    $endRecord = $pageNo * 10 + 10;
                @endphp
                <div class="dataTables_info">Showing {{$startRecord}} to {{$endRecord}} of {{$staff->total()}} entries</div>
                <div class="dataTables_paginate paging_simple_numbers">
                    {{ $staff->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
