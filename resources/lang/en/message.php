<?php

return [

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'staff_site_live_success' => '',
    'staff_site_live_failed' => '',
    'staff_round_robin_success' => '',
    'staff_round_robin_failed' => '',



];
